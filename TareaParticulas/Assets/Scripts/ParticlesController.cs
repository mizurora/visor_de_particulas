using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesController : MonoBehaviour
{
    public ParticleSystem snow;
   

    private void Start()
    {
        var emission = snow.emission;
       
        emission.enabled = false;
    }

    public void IncreaseParticles(int increment)
    {
        var emission = snow.emission;

        emission.rateOverTime = emission.rateOverTime.constant + increment;
    }

    public void switchOn()
    {
        var emission = snow.emission;

        emission.enabled = true;
    }

    public void switchOff()
    {
        var emission = snow.emission;

        emission.enabled = false;
        emission.rateOverTime = 100;
    }

    public void CambiarColor()
    {
        var principal= snow.main;
        principal.startColor=Color.red;
    }
}
